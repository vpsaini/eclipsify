'''
Created on Nov 20, 2013

@author: vaibhavsaini
'''
import os

class Eclipsify(object):
    '''
    classdocs
    '''


    def __init__(self, params):
        '''
        Constructor
        '''
        self.rootFolder = "/Users/vaibhavsaini/Dropbox/clonedetection/projects"
        pass
    
    def writeToFile(self, projectName):
        filename = "{0}/{1}/.project".format(self.rootFolder,projectName)
        print(filename)
        f = open(filename, 'w+')
        text = """<?xml version="1.0" encoding="UTF-8"?>
<projectDescription>
    <name>{0}</name>
    <comment></comment>
    <projects>
    </projects>
    <buildSpec>
        <buildCommand>
            <name>org.eclipse.jdt.core.javabuilder</name>
            <arguments>
            </arguments>
        </buildCommand>
    </buildSpec>
    <natures>
        <nature>org.eclipse.jdt.core.javanature</nature>
    </natures>
</projectDescription>""".format(projectName);
        f.write(text)
        f.close()

    def process(self):
        print "eclipsify bot at workp"
        subFolders = os.walk(self.rootFolder).next()[1]
        #print "root : {0}".format(root)
        count =0;
        for subFolder in subFolders:
            print subFolder
            self.writeToFile(subFolder)
            count+=1;
        return count

if __name__ == '__main__':
    eclipsify = Eclipsify({})
    count = eclipsify.process();
    msg = "{0} projects eclipsified :)".format(count)
    print msg